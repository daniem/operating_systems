#include <bitset>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <thread>

#include <fcntl.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/wait.h>

#include <unistd.h>
#include <sys/types.h>

using namespace std;

int* create_memory(char* name, int size) {
    int shm = shm_open(name, O_CREAT | O_RDWR | O_EXCL, 0600);

    if (shm == -1) {
        cerr << "Shared memory already created" << endl;
        return (int*)-1;
    }

    off_t size_mem = size;

    if (ftruncate(shm, size_mem) == -1) {
        cerr << "Problems with memory size" << endl;
        return (int*)-1;
    }

    char *pMem = static_cast<char*>(mmap(NULL, size_mem, PROT_READ | PROT_WRITE,
                                        MAP_SHARED, shm, 0));

    if ((void *) pMem == (void *) -1) {
        cerr << "Problems with memory map" << endl;
        return (int*)-1;
    }

    return (int*)pMem;
}

void readInput(int tuberiaIn[]) {
    string input;
    cout << "Waiting for input" << endl;
    while (getline(cin, input)) {
        int wv = write(tuberiaIn[1], &input[0], strlen(input.c_str()));
        if (wv < 0) {
            cerr << "WRITE ERROR FROM PIPE" << endl;
        }

        cout << wv << endl;
        cout << "Waiting for input" << endl;
    }
}

void writeOutput(int tuberiaOut[]) {
    char line[1000];

    int rv = read(tuberiaOut[0], line, 1000);

    while(rv) {
        if (rv == 0) {
            cerr << "Child Closed Pipe" << endl;
            break;
        }
        cout << "OUTPUT of CHILD PROGRAM is: " << line;

        rv = read(tuberiaOut[0], line, 1000);
    }
}

int main(int argc, char* argv[]) {
    if (argc < 4 && argv[1] != "-n") {
        cout << "Usage: " << argv[0] << " [-n <memshared>] [<cfgfile>]" << endl;
        return EXIT_FAILURE;
    }

    char* memname = argv[2];
    char* cfgfile = argv[3];

    ifstream file(cfgfile);
    string input;

    if (file) {
        string mewFilename;
        getline(file, mewFilename, ' ');
        ifstream is;
        size_t size;

        is.open(mewFilename, ios::binary | ios::in | ios::ate);

        if (!is.is_open()) {
            cerr << "Failed to open " << mewFilename << endl;
            return EXIT_FAILURE;
        }

        size = is.tellg();
        char* inputData = new char[size];

        is.seekg(0, ios::beg);
        is.read(inputData, size);
        inputData[size] = '\0';

        int* data = (int*)inputData;

        int* final_segment = data + 5;
        int final_base = (*final_segment >> 16) << 2;
        int final_limit = (*final_segment << 16) >> 16;
        int final_size = final_base + final_limit;

        int* pMem = create_memory(memname, final_size);
        int* confMem = create_memory((char*)"config", 28);

        *confMem = final_size;
        confMem = confMem + 1;

        for (int i = 0; i < 6; ++i) {
            int* segment = data + i;

            *(confMem + i) = *segment;
        }

        for (int i = 0; i < size; ++i) {
            *(pMem + i) = *(data + i);
        }

        while (getline(file, input, ' ')) {
            pid_t child = fork();

            if (child) {
                int status;
                waitpid(child, &status, 0);
            } else {
                vector<char*> arg_v;
                arg_v.push_back((char*)"/home/dmartinez/Code/universidad/operating_systems/build/bin/interewe");
                arg_v.push_back(memname);
                arg_v.push_back(&input[0]);
                execv(arg_v[0], &arg_v.front());
                return EXIT_SUCCESS;
            }
        }
    }

    file.close();
    
    return EXIT_SUCCESS;
}