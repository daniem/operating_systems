#include "memory.h"

Memory::Memory(char* newName, int size, std::vector<Segment> segments) {
    name = newName;
    pMem = init(name, size);
    create_mmu(segments);
    init_semaphores();

    if ((void*) pMem == (void*) -1) {
        std::cout << "Error opening memory" << std::endl;
    }
}        

char* Memory::init(char* name, int size) {
    int shm = shm_open(name, O_RDWR , 0600);

    if (shm == -1) {
        std::cerr << "Failed to open shared memory " << name << std::endl;
        return (char*)-1;
    } else {
        
    }

    off_t size_mem = (off_t) size;

    char* pMem = static_cast<char*>(mmap(NULL, size_mem, PROT_READ | PROT_WRITE,
                                        MAP_SHARED, shm, 0));

    if ((void *) pMem == (void *) -1) { 
        std::cerr << "Problems with memory map ("
            << errno << ") "
            << strerror(errno)
            << std::endl;
        return (char*)-1;
    }
    return pMem;
}

void Memory::init_semaphores() {
    int* memory = (int*) pMem;
    int offset = 10;

    std::vector<ProtectedRegion*> regionsVal;

    for (int i = 6; i < 10; ++i) {
        int segment = *(memory + i);

        int base = (segment >> 16);
        int limit = ((segment << 16) >> 16);

        int policyQuantity = limit - base;

        for(int j = 0; j < policyQuantity; j++){
            segment = *(memory + offset + j);

            base = (segment >> 16);
            limit = ((segment << 16) >> 16);

            std::string name;
            if (i == 6) {
                name += "LEL";
                name += std::to_string(j);
                const char* nameLec = (name + "L").c_str();
                const char* nameEsc = (name + "E").c_str();
                const char* namePri = (name + "P").c_str();
                
                sem_t *sem_lec = sem_open(nameLec, O_CREAT | O_EXCL, 0600, 1);
                sem_t *sem_esc = sem_open(nameEsc, O_CREAT | O_EXCL, 0600, 1);
                sem_t *sem_pri = sem_open(namePri, O_CREAT | O_EXCL, 0600, 1);

                std::vector<sem_t*> semaphores;

                semaphores.push_back(sem_lec);
                semaphores.push_back(sem_esc);
                semaphores.push_back(sem_pri);

                ProtectedLEL* region = new ProtectedLEL(base, limit, semaphores);
                regionsVal.push_back(region);
            } else if (i == 7) {
                name += "LEE";
                name += std::to_string(j);
                const char* nameLec = (name + "L").c_str();
                const char* nameEsc = (name + "E").c_str();
                const char* namePri = (name + "P").c_str();

                sem_t *sem_lec = sem_open(nameLec, O_CREAT | O_EXCL, 0600, 1);
                sem_t *sem_esc = sem_open(nameEsc, O_CREAT | O_EXCL, 0600, 1);
                sem_t *sem_pri = sem_open(namePri, O_CREAT | O_EXCL, 0600, 1);

                std::vector<sem_t*> semaphores;

                semaphores.push_back(sem_lec);
                semaphores.push_back(sem_esc);
                semaphores.push_back(sem_pri);

                ProtectedLEE* region = new ProtectedLEE(base, limit, semaphores);
                regionsVal.push_back(region);
            } else if (i == 8) {
                name += "B";
                name += std::to_string(j);
                const char* nameMutex = name.c_str();

                sem_t *sem_mutex = sem_open(nameMutex, O_CREAT | O_EXCL, 0600, 1);

                std::vector<sem_t*> semaphores;

                semaphores.push_back(sem_mutex);

                ProtectedB* region = new ProtectedB(base, limit, semaphores);
                regionsVal.push_back(region);
            } else {
                name += "NB";
                name += std::to_string(j);
                std::vector<sem_t*> semaphores;

                ProtectedNB* region = new ProtectedNB(base, limit, semaphores);                   
                regionsVal.push_back(region);
            }
        }
        offset += policyQuantity;
    }

    regions = regionsVal;
}

void Memory::writeMemory(int position, char value) {
    bool flag = false;
    for (int i = 0; i < regions.size(); ++i) {
        if (regions[i]->base > position && regions[i]->limit < position) {
            regions[i]->storeValue(pMem + position, value);
            flag = true;
            break;
        }
    }

    if (!flag) std::cout << "Segmentation fault papá" << std::endl;
}

void Memory::writeMemory(int position, int value) {
    bool flag = false;
    for (int i = 0; i < regions.size(); ++i) {
        if (regions[i]->base > position && regions[i]->limit < position) {
            regions[i]->storeValue(pMem + position, value);
            flag = true;
            break;
        }
    }

    if (!flag) std::cout << "Segmentation fault papá" << std::endl;
}

// char Memory::readMemory(){

// }

void Memory::create_mmu(std::vector<Segment> segments) {
    for (int i = 0; i < 6; ++i) {
        mmu.table.insert(std::pair<int, struct Segment>(i, segments[i]));
    }
}

void Memory::writeChar(int segment, int offset, char value) {
    int real = mmu.getPositionReal(segment, offset);

    if (real == -1) {
        std::cerr << "Wrong memory position writing char" << std::endl;
    } else {
        // writeMemory(real, value);
        *(pMem + real) = value;
    }
}

char Memory::readChar(int segment, int offset) {
    int real = mmu.getPositionReal(segment, offset);
    if (real == -1) {
        std::cerr << "Wrong memory position reading char" << std::endl;
        return -1;
    }

    return *(pMem + real);
}

void Memory::writeInt(int segment, char* offset, int value) {
    int movement = strtol(offset, nullptr, 2);

    movement <<= 2;

    int real = mmu.getPositionReal(segment, movement);

    if (real == -1) {
        std::cerr << "Wrong memory position writing int" << std::endl;
    } else {
        // writeMemory(real, value);
        int* pIntMem = (int*) (pMem + real);
        *pIntMem = value;
    }
}

int Memory::readInt(int segment, char* offset) {
    int movement = strtol(offset, nullptr, 2);

    movement <<= 2;

    int real = mmu.getPositionReal(segment, movement);

    if (real == -1) {
        std::cerr << "Wrong memory position reading int" << std::endl;
        return -1;
    }

    return *((int*) (pMem + real));
}