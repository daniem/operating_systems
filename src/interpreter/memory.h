#include <bitset>
#include <cerrno>
#include <cstring>
#include <iostream>
#include <fcntl.h>
#include <map>
#include <semaphore.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <vector>

struct Segment {
    int base;
    int limit;
    bool isInside(int position) {
        return position < limit;
    }
};

struct MMU {
    std::map<int, Segment> table;
    int getPositionReal(int segmentNumber, int movement) {
        Segment segment = table[segmentNumber];
        if (segment.isInside(movement)) {
            return segment.base + movement;
        } else {
            return -1;
        }
    }
};

struct ProtectedRegion {
    int base;
    int limit;
    int count;

    std::vector<sem_t*> semaphores;

    ProtectedRegion(int baseVal, int limitVal, std::vector<sem_t*> semaphoresVal) {
        base = baseVal;
        limit = limitVal;
        semaphores = semaphoresVal;
    }
    virtual void storeValue(char* position, char value) {}
    virtual void storeValue(char* position, int value) {}
    virtual char readValueChar(char* position) {}
    virtual int readValueInt(char* position) {}
};

struct ProtectedLEL : public ProtectedRegion {
    ProtectedLEL(int base, int limit, std::vector<sem_t*> semaphores) : ProtectedRegion(base, limit, semaphores){ }

    void storeValue(char* position, char value) {
        sem_t* s_escritor = semaphores[1];
        sem_t* s_prioridad = semaphores[2];

        sem_wait(s_prioridad);
        sem_wait(s_escritor);
        
        *position = value;

        sem_post(s_prioridad);
        sem_post(s_escritor);
    }

    void storeValue(char* position, int value) {
        sem_t* s_escritor = semaphores[1];
        sem_t* s_prioridad = semaphores[2];

        sem_wait(s_prioridad);
        sem_wait(s_escritor);
        
        int* pIntMem = (int*) position;
        *pIntMem = value;

        sem_post(s_prioridad);
        sem_post(s_escritor);
    }

    char readValueChar(char* position) {
        sem_t* s_lector = semaphores[0];
        sem_t* s_escritor = semaphores[1];
        sem_t* s_prioridad = semaphores[2];

        sem_wait(s_lector);
        if(true){
            sem_wait(s_prioridad);
        }
        sem_post(s_lector);
        sem_wait(s_escritor);
        char value = *position;

        sem_wait(s_lector);
        if(true){
            sem_post(s_prioridad);
        }
        sem_post(s_lector);
        sem_post(s_escritor);
        return value;
    }

    int readValueInt(char* position) {
        sem_t* s_lector = semaphores[0];
        sem_t* s_escritor = semaphores[1];
        sem_t* s_prioridad = semaphores[2];

        sem_wait(s_lector);
        if(true){
            sem_wait(s_prioridad);
        }
        sem_post(s_lector);
        sem_wait(s_escritor);
        int value = *((int*) position);

        sem_wait(s_lector);
        if(true){
            sem_post(s_prioridad);
        }
        sem_post(s_lector);
        sem_post(s_escritor);
        return value;
    }
};

struct ProtectedLEE : public ProtectedRegion {
    ProtectedLEE(int base, int limit, std::vector<sem_t*> semaphores) : ProtectedRegion(base, limit, semaphores){ }
    void storeValue(char* position, char value) {
        sem_t* s_lector = semaphores[0];
        sem_t* s_escritor = semaphores[1];
        sem_t* s_prioridad = semaphores[2];

        sem_wait(s_escritor);
       
        if(true){
            sem_wait(s_prioridad);
        }
        sem_post(s_escritor);
        sem_wait(s_lector);

        *position = value;

        sem_wait(s_escritor);
        if(true){
            sem_post(s_prioridad);
        }
        sem_post(s_escritor);
        sem_post(s_lector);
    }

    void storeValue(char* position, int value) {
        sem_t* s_lector = semaphores[0];
        sem_t* s_escritor = semaphores[1];
        sem_t* s_prioridad = semaphores[2];

        sem_wait(s_escritor);
       
        if(true){
            sem_wait(s_prioridad);
        }
        sem_post(s_escritor);
        sem_wait(s_lector);

        int* pInt = (int*) position;
        *pInt = value;

        sem_wait(s_escritor);
        if(true){
            sem_post(s_prioridad);
        }
        sem_post(s_escritor);
        sem_post(s_lector);
    }

    char readValueChar(char* position) {
        sem_t* s_lector = semaphores[0];
        sem_t* s_prioridad = semaphores[2];
        sem_wait(s_prioridad);
        sem_wait(s_lector);
        
        char res = *position;

        sem_post(s_prioridad);
        sem_post(s_lector);
        return res;
    }

    int readValueInt(char* position) {
        sem_t* s_lector = semaphores[0];
        sem_t* s_prioridad = semaphores[2];
        sem_wait(s_lector);
        sem_wait(s_prioridad);
        
        int value = *((int *) position);

        sem_post(s_prioridad);
        sem_post(s_lector);
        return value;
    }
};

struct ProtectedB : public ProtectedRegion {
    ProtectedB(int base, int limit, std::vector<sem_t*> semaphores) : ProtectedRegion(base, limit, semaphores){ }
    void storeValue(char* position, char value) {
        sem_t* s_mutex = semaphores[0];

        sem_wait(s_mutex);
        
        *position = value;
        
        sem_post(s_mutex);
    }

    void storeValue(char* position, int value) {
        sem_t* s_mutex = semaphores[0];

        sem_wait(s_mutex);
        int* pInt = (int*) position;
        *(pInt) = value;
        sem_post(s_mutex);
    }

    char readValueChar(char* position) {
        sem_t* s_mutex = semaphores[0];
        
        sem_wait(s_mutex);
        
        char value = *position;

        sem_post(s_mutex);
        return value;
    }

    int readValueInt(char* position) {
        sem_t* s_mutex = semaphores[0];

        sem_wait(s_mutex);
        int value = *((int*) position);
        sem_post(s_mutex);

        return value;
    }
};

struct ProtectedNB : public ProtectedRegion {
    ProtectedNB(int base, int limit, std::vector<sem_t*> semaphores) : ProtectedRegion(base, limit, semaphores){ }
    void storeValue(char* position, char value) {
        *position = value;
    }

    void storeValue(char* position, int value) {
        int* pInt = (int*) position;
        *(pInt) = value;
    }

    char readValueChar(char* position) {
        char value = *position;
        return value;
    }

    int readValueInt(char* position) {
        int value = *((int*) position);
        return value;
    }
};

class Memory {
    private:
        char* name;
        char* pMem;
        struct MMU mmu;
        std::vector<ProtectedRegion*> regions;

        char* init(char* name, int size);
        void init_semaphores();
        void create_mmu(std::vector<Segment> segments);
    
    public:
        Memory(char* newName, int size, std::vector<Segment> segments);
        void writeMemory(int position, char value);
        void writeMemory(int position, int value);
        int readInt(int segment, char* offset);
        void writeInt(int segment, char* offset, int value);
        char readChar(int segment, int offset);
        void writeChar(int segment, int offset, char value);
};