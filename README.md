# Interpretador Paralelo de BinEWE

Práctica Sistemas operativos 2018-1

### Prerequisitos

- g++ >= 8.1

### Compilación

Para compilar el programa se deben correr los siguientes comandos en la terminal:

```
make controlewe
make interewe
```


## Uso

```
./controlewe [-n <sharedmemname>] [<cfgfile>]
```


## Construido con

* [C++](https://isocpp.org) 
* Sudor
* Lágrimas


## Autores

* **Daniel Alejandro Martinez Montealegre** 
* **Samuel Eduardo Sarabia Osorio** 
* **Simon Gomez Uribe** 

## Profesor

* **Juan Guillermo Lalinde Pulido** 